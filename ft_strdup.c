/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 19:49:03 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 22:02:50 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_strdup(const char *s1)
{
	char	*res_array;

	if ((res_array = (char*)malloc(sizeof(*s1) * (ft_strlen(s1) + 1))))
		return (ft_strcpy(res_array, s1));
	else
		return (NULL);
}
