/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_head.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 18:43:24 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 18:43:25 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	check_conversion(char *traverse)
{
	while (ft_strchr("sSpdDioOuUxXcC", *traverse) == NULL)
	{
		if ((*traverse == '%' || *traverse == '\0')
			|| (ft_isalpha(*traverse) && ft_strchr("hljz", *traverse) == NULL))
			return (0);
		traverse++;
	}
	if (ft_strchr("sSpdDioOuUxXcC", *traverse) && *traverse != '\0')
		return (1);
	return (0);
}

void		pf_head(char **traverse, va_list arg)
{
	t_defarg *des_arg;

	des_arg = (t_defarg*)malloc(sizeof(t_defarg));
	ft_init_struct_elem_to_zero(des_arg);
	if (check_conversion(*traverse) == 1)
		ft_conversion_head(traverse, arg, des_arg);
	else
		ft_undefined_behaviour(traverse, arg, des_arg);
	free(des_arg);
}
