/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pos_decemic_plus_space_print.c                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 20:50:21 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 20:50:24 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_pos_decemic_plus_space_print(t_defarg *des_arg)
{
	char	*dig_print;
	char	*temp;

	if (des_arg->flag_plus == 1)
		temp = ft_memset(ft_strnew(1), '+', 1);
	else if (des_arg->flag_space == 1)
		temp = ft_memset(ft_strnew(1), ' ', 1);
	dig_print = ft_strjoin(temp, des_arg->arg_str);
	free(temp);
	while (des_arg->wid > (int)ft_strlen(dig_print))
	{
		ft_putchar_g(' ');
		des_arg->wid--;
	}
	ft_putstr_g(dig_print);
	ft_strdel(&dig_print);
}
