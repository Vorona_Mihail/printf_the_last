/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   diD_convrsions_arg_to_str.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/18 15:22:04 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 21:38:00 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_defarg	*ft_check_did_write_arg_str(va_list arg, t_defarg *des_arg)
{
	if (des_arg->modif == NULL && des_arg->conv != 'D')
		des_arg->arg_str = ft_itoa(va_arg(arg, int));
	else if (ft_strcmp(des_arg->modif, "hh") == 0)
		des_arg->arg_str = ft_itoa((int)(char)va_arg(arg, int));
	else if (ft_strcmp(des_arg->modif, "h") == 0)
		des_arg->arg_str = ft_itoa((int)(short int)va_arg(arg, int));
	else if (ft_strcmp(des_arg->modif, "ll") == 0)
		des_arg->arg_str = ft_itoa_base((long long)va_arg(arg, long long), 10);
	else if (ft_strcmp(des_arg->modif, "l") == 0 || des_arg->conv == 'D')
		des_arg->arg_str = ft_itoa_base((long long)va_arg(arg, long int), 10);
	else if (ft_strcmp(des_arg->modif, "j") == 0)
		des_arg->arg_str = ft_itoa_base((long long)va_arg(arg, intmax_t), 10);
	else if (ft_strcmp(des_arg->modif, "z") == 0)
		des_arg->arg_str = ft_itoa_base((long long)va_arg(arg, size_t), 10);
	return (des_arg);
}

t_defarg	*ft_write_arg_str(va_list arg, t_defarg *des_arg, int base)
{
	if (ft_strchr("uoxX", des_arg->conv) && des_arg->modif == NULL)
		des_arg->arg_str = ft_itoa_base_uns(
						(unsigned long long)va_arg(arg, unsigned int), base);
	else if (ft_strcmp(des_arg->modif, "hh") == 0)
		des_arg->arg_str = ft_itoa_base_uns((unsigned long long)
							(unsigned char)va_arg(arg, unsigned int), base);
	else if (ft_strcmp(des_arg->modif, "h") == 0)
		des_arg->arg_str = ft_itoa_base_uns(
		(unsigned long long)(unsigned short)va_arg(arg, unsigned int), base);
	else if (ft_strcmp(des_arg->modif, "ll") == 0)
		des_arg->arg_str = ft_itoa_base_uns(va_arg(
												arg, unsigned long long), base);
	else if (!ft_strcmp(des_arg->modif, "l") || ft_strchr("OU", des_arg->conv))
		des_arg->arg_str = ft_itoa_base_uns(
						(unsigned long long)va_arg(arg, unsigned long), base);
	else if (ft_strcmp(des_arg->modif, "j") == 0)
		des_arg->arg_str = ft_itoa_base_uns(
				(unsigned long long)va_arg(arg, uintmax_t), base);
	else if (ft_strcmp(des_arg->modif, "z") == 0)
		des_arg->arg_str = ft_itoa_base_uns(
							(unsigned long long)va_arg(arg, size_t), base);
	if (des_arg->conv == 'x')
		des_arg->arg_str = ft_tolowstr(des_arg->arg_str);
	return (des_arg);
}

t_defarg	*ft_check_write_arg_str(va_list arg, t_defarg *des_arg)
{
	int base;

	base = 10;
	if (ft_strchr("diD", des_arg->conv) != NULL)
		ft_check_did_write_arg_str(arg, des_arg);
	else if (ft_strchr("uUoOxX", des_arg->conv))
	{
		if (des_arg->conv == 'u' || des_arg->conv == 'U')
			base = 10;
		else if (des_arg->conv == 'o' || des_arg->conv == 'O')
			base = 8;
		else if (des_arg->conv == 'x' || des_arg->conv == 'X')
			base = 16;
		ft_write_arg_str(arg, des_arg, base);
	}
	return (des_arg);
}
