/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   digital_print_actions.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 18:58:16 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 20:31:39 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		ft_add_prefix(t_defarg *des_arg)
{
	if (ft_strchr("oO", des_arg->conv) && des_arg->arg_str[0] != '0')
		ft_putchar_g('0');
	else if (des_arg->conv == 'x' && des_arg->arg_str[0] != '0')
		ft_putstr_g("0x");
	else if (des_arg->conv == 'X' && des_arg->arg_str[0] != '0')
		ft_putstr_g("0X");
}

static char		*ft_new_string(t_defarg *des_arg)
{
	char	*tmp;
	int		diff;
	char	*dig_print;

	if ((diff = des_arg->prec - (int)ft_strlen(des_arg->arg_str)) >= 0)
	{
		dig_print = ft_strnew(diff);
		ft_memset(dig_print, '0', diff);
		tmp = dig_print;
		dig_print = ft_strjoin(dig_print, des_arg->arg_str);
		free(tmp);
	}
	else
		dig_print = ft_strdup(des_arg->arg_str);
	return (dig_print);
}

static void		ft_utility_uns_dec(t_defarg *des_arg, char *dig_print)
{
	if (des_arg->prec == 0
			&& des_arg->arg_str[0] == '0' && ft_strchr("oOxX", des_arg->conv))
	{
		if (des_arg->wid > 0)
			ft_putchar_g(' ');
		else if (des_arg->wid < 0 && ft_strchr("oO", des_arg->conv)
				&& des_arg->flag_sharp == 1)
			ft_putchar_g('0');
	}
	else if (des_arg->flag_minus == 0)
	{
		if (!(ft_strchr("uU", des_arg->conv) && des_arg->arg_str[0] == '0'
													&& des_arg->prec == 0))
			ft_putstr_g(dig_print);
	}
}

static void		ft_uns_decemic_print_actions(t_defarg *des_arg)
{
	char	*dig_print;

	dig_print = ft_new_string(des_arg);
	if (des_arg->flag_minus == 1 || des_arg->flag_zero == 1)
	{
		if (des_arg->flag_sharp == 1)
			ft_add_prefix(des_arg);
		if (des_arg->flag_minus == 1)
			ft_putstr_g(dig_print);
	}
	while (des_arg->wid > (int)ft_strlen(dig_print))
	{
		if (des_arg->flag_zero == 0 || des_arg->flag_minus == 1)
			ft_putchar_g(' ');
		else
			ft_putchar_g('0');
		des_arg->wid--;
	}
	if (ft_strchr("oOxX", des_arg->conv) && des_arg->flag_sharp == 1
			&& des_arg->flag_minus == 0 && des_arg->flag_zero == 0
			&& !(des_arg->wid <= 0 && ft_strchr("oO", des_arg->conv)
			&& des_arg->prec == (int)ft_strlen(dig_print)))
		ft_add_prefix(des_arg);
	ft_utility_uns_dec(des_arg, dig_print);
	ft_strdel(&dig_print);
}

void			ft_digital_print_actions(t_defarg *des_arg)
{
	if (des_arg->prec >= 0)
		des_arg->flag_zero = 0;
	if (ft_strchr("diD", des_arg->conv))
		ft_decemic_int_print_actions(des_arg);
	else if (ft_strchr("oOxX", des_arg->conv))
	{
		if (ft_strchr("xX", des_arg->conv)
						&& des_arg->wid > (int)ft_strlen(des_arg->arg_str)
						&& des_arg->flag_sharp == 1
						&& des_arg->arg_str[0] != '0')
			des_arg->wid -= 2;
		if (ft_strchr("oO", des_arg->conv)
						&& des_arg->wid > (int)ft_strlen(des_arg->arg_str)
						&& des_arg->flag_sharp == 1
						&& des_arg->arg_str[0] != '0')
			des_arg->wid--;
		ft_uns_decemic_print_actions(des_arg);
	}
	else if (ft_strchr("uU", des_arg->conv))
		ft_uns_decemic_print_actions(des_arg);
}
