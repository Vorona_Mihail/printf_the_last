/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unbehaviour_print.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 18:45:49 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 18:45:50 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_unbeh_zero_space_print(t_defarg *des_arg)
{
	char	symb;

	if (des_arg->flag_zero == 1)
		symb = '0';
	else
		symb = ' ';
	while (des_arg->wid - 1 > 0)
	{
		ft_putchar_g(symb);
		des_arg->wid--;
	}
}

void		ft_unbehaviour_print(t_defarg *des_arg)
{
	if (des_arg->flag_minus == 1)
		des_arg->flag_zero = 0;
	if (des_arg->flag_minus == 1)
	{
		ft_putstr_g(des_arg->arg_str);
		ft_unbeh_zero_space_print(des_arg);
	}
	else
	{
		ft_unbeh_zero_space_print(des_arg);
		ft_putstr_g(des_arg->arg_str);
	}
}
