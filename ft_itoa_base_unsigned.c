/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base_unsigned.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/07 15:05:50 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/01 18:07:46 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static unsigned int	ft_len_and_digit(unsigned long long num, int base,
										unsigned long long *digit)
{
	unsigned int	i;

	i = 0;
	*digit = num;
	if (num == 0)
		i++;
	while (num > 0)
	{
		i++;
		num /= (unsigned long long)base;
	}
	return (i);
}

static char			*ft_make_str(char *str, unsigned int i,
										unsigned long long digit, int base)
{
	char			*represent;

	represent = "0123456789ABCDEF";
	while (digit > 0)
	{
		str[i--] = represent[digit % (unsigned long long)base];
		digit /= (unsigned long long)base;
	}
	return (str);
}

char				*ft_itoa_base_uns(unsigned long long num, int base)
{
	char					*str_num;
	unsigned int			i;
	unsigned long long		digit;

	if (base < 2 && base > 16)
		return (NULL);
	i = ft_len_and_digit(num, base, &digit);
	if ((str_num = (char*)malloc(sizeof(char) * i + 1)))
	{
		str_num[i--] = '\0';
		if (num == 0)
			str_num[0] = '0';
		else
			ft_make_str(str_num, i, digit, base);
	}
	return (str_num);
}
