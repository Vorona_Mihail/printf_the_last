/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_str_chr_argum_int_str.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/18 20:01:35 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 19:16:27 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*ft_strdup_mod(const char *s1)
{
	char	*res_arr;

	if (!s1)
	{
		res_arr = (char*)malloc(sizeof(char) * 7);
		res_arr = "(null)";
		return (res_arr);
	}
	else if ((res_arr = (char*)malloc(sizeof(*s1) * (ft_strlen(s1) + 1))))
		return (ft_strcpy(res_arr, s1));
	return (NULL);
}

t_defarg	*ft_put_chr_arg_in_str_head(va_list arg, t_defarg *des_arg)
{
	if (des_arg->conv == 'S')
		des_arg->modif = ft_strdup("l");
	if (des_arg->conv == 's' && des_arg->modif == NULL)
		des_arg->arg_str = ft_strdup_mod(va_arg(arg, char*));
	else if (des_arg->conv == 'c' && des_arg->modif == NULL)
		des_arg->arg_str = ft_memset(ft_strnew(1), ((char)va_arg(arg, int)), 1);
	else if ((des_arg->conv == 'c' && ft_strcmp(des_arg->modif, "l") == 0)
													|| des_arg->conv == 'C')
		des_arg->arg_str = ft_memset(ft_strnew(1),
										((unsigned char)va_arg(arg, int)), 1);
	else if (ft_strcmp(des_arg->modif, "l") == 0
										&& ft_strchr("sS", des_arg->conv))
		des_arg->arg_str = ft_strdup_wint(va_arg(arg, wint_t*));
	return (des_arg);
}
