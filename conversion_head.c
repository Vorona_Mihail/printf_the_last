/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conversion_head.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 18:43:09 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 21:01:48 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_fill_struct_str(char **traverse, t_defarg *des_arg)
{
	int		i;
	char	*trav_head;
	int		b;

	trav_head = *traverse;
	i = 0;
	b = 0;
	while (ft_strchr("sSpdDioOuUxXcC", **traverse) == NULL)
	{
		i++;
		(*traverse)++;
	}
	des_arg->conv = **traverse;
	des_arg->str = (char*)malloc(sizeof(char) * (i + 2));
	while (b <= i)
		des_arg->str[b++] = *(trav_head++);
	des_arg->str[b] = '\0';
}

static void	ft_fill_flags(t_defarg *des_arg)
{
	ft_zero_flag(des_arg);
	ft_sharp_flag(des_arg);
	ft_space_flag(des_arg);
	ft_minus_flag(des_arg);
	ft_plus_flag(des_arg);
}

void		ft_conversion_head(char **traverse, va_list arg, t_defarg *des_arg)
{
	ft_fill_struct_str(traverse, des_arg);
	ft_fill_flags(des_arg);
	ft_fill_precision(des_arg);
	ft_fill_width(des_arg);
	if (ft_strchr("CS", des_arg->conv) == NULL)
		ft_fill_modificators(des_arg);
	ft_conversion_actions_head(arg, des_arg);
	ft_print_arg(des_arg);
	if (ft_strchr("sS", des_arg->conv) == NULL
								&& ft_strlen(des_arg->arg_str) == 0)
		g_return++;
}
