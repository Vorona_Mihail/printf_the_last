/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   neg_decemic_print.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 19:07:00 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 19:23:48 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		ft_neg_decemic_print_minus(t_defarg *des_arg)
{
	char	*dig_print;

	dig_print = ft_neg_minus_util_arr(des_arg);
	ft_putstr_g(dig_print);
	while (des_arg->wid > (int)ft_strlen(dig_print))
	{
		ft_putchar_g(' ');
		des_arg->wid--;
	}
	ft_strdel(&dig_print);
}

static void	ft_utility_decemic_func(t_defarg *des_arg, char *dig_print)
{
	if (des_arg->flag_zero == 0)
	{
		while (des_arg->wid > (int)ft_strlen(dig_print))
		{
			ft_putchar_g(' ');
			des_arg->wid--;
		}
	}
	else if (des_arg->flag_zero == 1)
	{
		ft_putchar_g('-');
		des_arg->wid--;
		while (des_arg->wid > (int)ft_strlen(dig_print))
		{
			ft_putchar_g('0');
			des_arg->wid--;
		}
	}
}

void		ft_neg_decemic_print(t_defarg *des_arg)
{
	char	*dig_print;

	if (des_arg->prec < (int)ft_strlen(des_arg->arg_str)
											&& des_arg->flag_zero == 0)
		dig_print = ft_strdup(des_arg->arg_str);
	else
		dig_print = ft_neg_util_arr(des_arg);
	ft_utility_decemic_func(des_arg, dig_print);
	ft_putstr_g(dig_print);
	ft_strdel(&dig_print);
}
