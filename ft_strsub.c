/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 21:50:44 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 22:03:58 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char		*new_arr;
	size_t		i;

	i = 0;
	if (s)
	{
		new_arr = (char*)malloc(sizeof(*s) * (len + 1));
		if (new_arr)
		{
			while (i < len)
			{
				new_arr[i] = s[start];
				start++;
				i++;
			}
			new_arr[i] = '\0';
			return (new_arr);
		}
		return (NULL);
	}
	else
		return (NULL);
}
