/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags_funcs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/16 15:22:05 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 20:35:13 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_defarg	*ft_zero_flag(t_defarg *des_arg)
{
	int		i;
	int		flag;

	i = 0;
	flag = 0;
	while (des_arg->str[i] != des_arg->conv)
	{
		if (des_arg->str[i - 1] == '.' && ft_isdigit(des_arg->str[i]))
		{
			while (ft_isdigit(des_arg->str[i]))
				i++;
			i--;
			flag = 1;
		}
		if (des_arg->str[i] == '0' && !ft_isdigit(des_arg->str[i - 1])
															&& flag == 0)
		{
			des_arg->flag_zero = 1;
			break ;
		}
		flag = 0;
		i++;
	}
	return (des_arg);
}

t_defarg	*ft_sharp_flag(t_defarg *des_arg)
{
	int		i;

	i = 0;
	while (des_arg->str[i] != des_arg->conv)
	{
		if (des_arg->str[i] == '#')
		{
			des_arg->flag_sharp = 1;
			break ;
		}
		i++;
	}
	return (des_arg);
}

t_defarg	*ft_space_flag(t_defarg *des_arg)
{
	int		i;

	i = 0;
	while (des_arg->str[i] != des_arg->conv)
	{
		if (des_arg->str[i] == ' ')
		{
			des_arg->flag_space = 1;
			break ;
		}
		i++;
	}
	return (des_arg);
}

t_defarg	*ft_minus_flag(t_defarg *des_arg)
{
	int		i;

	i = 0;
	while (des_arg->str[i] != des_arg->conv)
	{
		if (des_arg->str[i] == '-')
		{
			des_arg->flag_minus = 1;
			break ;
		}
		i++;
	}
	return (des_arg);
}

t_defarg	*ft_plus_flag(t_defarg *des_arg)
{
	int		i;

	i = 0;
	while (des_arg->str[i] != des_arg->conv)
	{
		if (des_arg->str[i] == '+')
		{
			des_arg->flag_plus = 1;
			break ;
		}
		i++;
	}
	return (des_arg);
}
