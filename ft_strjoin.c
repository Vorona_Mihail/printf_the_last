/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 14:56:23 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 22:03:05 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char			*new_arr;
	size_t			i;
	size_t			j;

	i = 0;
	j = 0;
	if (s1 && s2)
	{
		if ((new_arr = (char*)malloc(sizeof(char) * (ft_strlen(s1)
							+ ft_strlen(s2) + 1))))
		{
			while (s1[i])
			{
				new_arr[i] = s1[i];
				i++;
			}
			while (s2[j])
				new_arr[i++] = s2[j++];
			new_arr[i] = '\0';
			return (new_arr);
		}
		else
			return (NULL);
	}
	return (NULL);
}
