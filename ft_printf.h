/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/03 14:25:16 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 22:32:36 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <unistd.h>
# include <stdarg.h>
# include <wchar.h>
# include <stdlib.h>
# include <stdint.h>
# include <string.h>

int g_return;

typedef	struct	s_defarg
{
	char		*str;
	int			conv;
	char		*modif;
	int			wid;
	int			prec;
	int			flag_zero;
	int			flag_sharp;
	int			flag_space;
	int			flag_minus;
	int			flag_plus;
	char		*arg_str;
	wchar_t		*l_string;
}				t_defarg;

t_defarg		*ft_init_struct_elem_to_zero(t_defarg *des_arg);
t_defarg		*ft_zero_flag(t_defarg *des_arg);
t_defarg		*ft_sharp_flag(t_defarg *des_arg);
t_defarg		*ft_space_flag(t_defarg *des_arg);
t_defarg		*ft_minus_flag(t_defarg *des_arg);
t_defarg		*ft_plus_flag(t_defarg *des_arg);
t_defarg		*ft_fill_precision(t_defarg *des_arg);
t_defarg		*ft_fill_width(t_defarg *des_arg);
t_defarg		*ft_fill_modificators(t_defarg *des_arg);
t_defarg		*ft_put_digital_arg_in_str_head(va_list arg, t_defarg *des_arg);
t_defarg		*ft_check_did_write_arg_str(va_list arg, t_defarg *des_arg);
t_defarg		*ft_check_write_arg_str(va_list arg, t_defarg *des_arg);
t_defarg		*ft_write_arg_str(va_list arg, t_defarg *des_arg, int bse);
t_defarg		*ft_put_chr_arg_in_str_head(va_list arg, t_defarg *des_arg);
void			ft_free_struct(t_defarg *des_arg);
void			ft_unbehaviour_print(t_defarg *des_arg);
void			ft_undefined_behaviour(char **traverse,
											va_list arg, t_defarg *des_arg);
void			ft_pointer_address_func(va_list arg, t_defarg *des_arg);
void			ft_pos_decemic_plus_space_print(t_defarg *des_arg);
char			*ft_neg_util_arr(t_defarg *des_arg);
char			*ft_neg_minus_util_arr(t_defarg *des_arg);
char			*ft_pos_minus_util_arr(t_defarg *des_arg);
char			*ft_pos_util_arr(t_defarg *des_arg);
void			ft_print_plus_space(t_defarg *des_arg);
void			ft_print_zero_space(t_defarg *des_arg);
void			ft_pos_decemic_minus_print(t_defarg *des_arg);
void			ft_pos_decemic_print(t_defarg *des_arg);
void			ft_neg_decemic_print_minus(t_defarg *des_arg);
void			ft_neg_decemic_print(t_defarg *des_arg);
void			ft_decemic_int_print_actions(t_defarg *des_arg);
void			ft_str_print_actions(t_defarg *des_arg);
void			ft_chr_print_actions(t_defarg *des_arg);
void			ft_digital_print_actions(t_defarg *des_arg);
void			ft_print_arg(t_defarg *des_arg);
void			ft_conversion_actions_head(va_list arg, t_defarg *des_arg);
char			*ft_strdup_wint(wint_t *str);
int				ft_printf(char *argum, ...);
void			ft_putchar_g(char c);
void			ft_putstr_g(char const *s);
void			pf_head(char **traverse, va_list arg);
void			ft_conversion_head(char **traverse,
											va_list arg, t_defarg *des_arg);
char			*ft_conversions(char **traverse, va_list arg);
void			ft_putnbr_uns(unsigned int n);
char			*ft_tolowstr(char *str);
char			*ft_itoa_base(long long num, int base);
char			*ft_itoa_base_uns(unsigned long long num, int base);
void			*ft_memset(void *b, int c, size_t len);
size_t			ft_strlen(const char *s);
char			*ft_strdup(const char *s1);
char			*ft_strchr(const char *s, int c);
char			*ft_strrchr(const char *s, int c);
char			*ft_strstr(const char *big, const char *little);
int				ft_atoi(const char *str);
int				ft_isalpha(int c);
int				ft_isdigit(int c);
int				ft_tolower(int c);
char			*ft_strnew(size_t size);
char			*ft_strsub(char const *s, unsigned int start, size_t len);
char			*ft_strjoin(char const *s1, char const *s2);
char			*ft_itoa(int n);
char			*ft_strcpy(char *dst, const char *src);
void			ft_putchar(char c);
void			ft_strdel(char **as);
int				ft_strcmp(const char *s1, const char *s2);

#endif
