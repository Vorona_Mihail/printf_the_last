/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pointer_adress_func.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 20:49:46 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 20:49:48 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		ft_add_prec_to_argument(t_defarg *des_arg)
{
	char	*tmp;
	char	*zero_arr;

	tmp = des_arg->arg_str;
	zero_arr = ft_strnew((size_t)des_arg->prec - ft_strlen(des_arg->arg_str));
	ft_memset(zero_arr, '0',
						(size_t)des_arg->prec - ft_strlen(des_arg->arg_str));
	des_arg->arg_str = ft_strjoin(zero_arr, des_arg->arg_str);
	free(tmp);
	free(zero_arr);
}

static void		ft_print_null_space(t_defarg *des_arg)
{
	if (des_arg->arg_str[0] == '0' && des_arg->prec == 0 && des_arg->wid > 0)
	{
		if (des_arg->flag_zero == 1)
			ft_putchar_g('0');
		else
			ft_putchar_g(' ');
	}
}

static void		ft_print_address_with_minus(t_defarg *des_arg)
{
	ft_putstr_g("0x");
	des_arg->wid -= 2;
	if (ft_strcmp("0", des_arg->arg_str) == 0 && des_arg->prec == 0)
		ft_print_null_space(des_arg);
	else
		ft_putstr_g(des_arg->arg_str);
	while (des_arg->wid > (int)ft_strlen(des_arg->arg_str))
	{
		ft_putchar_g(' ');
		des_arg->wid--;
	}
}

static void		ft_print_address(t_defarg *des_arg)
{
	if (des_arg->flag_zero == 1)
	{
		ft_putstr_g("0x");
		while (des_arg->wid - 2 > (int)ft_strlen(des_arg->arg_str))
		{
			ft_putchar_g('0');
			des_arg->wid--;
		}
	}
	else
	{
		while (des_arg->wid - 2 > (int)ft_strlen(des_arg->arg_str))
		{
			ft_putchar_g(' ');
			des_arg->wid--;
		}
		ft_putstr_g("0x");
	}
	if (ft_strcmp("0", des_arg->arg_str) == 0 && des_arg->prec == 0)
		ft_print_null_space(des_arg);
	else
		ft_putstr_g(des_arg->arg_str);
}

void			ft_pointer_address_func(va_list arg, t_defarg *des_arg)
{
	des_arg->arg_str = ft_itoa_base_uns(va_arg(arg, uintmax_t), 16);
	ft_tolowstr(des_arg->arg_str);
	if (des_arg->prec > 0 || des_arg->flag_minus == 1)
		des_arg->flag_zero = 0;
	if (des_arg->prec > (int)ft_strlen(des_arg->arg_str))
		ft_add_prec_to_argument(des_arg);
	if (des_arg->flag_minus == 1)
		ft_print_address_with_minus(des_arg);
	else
		ft_print_address(des_arg);
}
