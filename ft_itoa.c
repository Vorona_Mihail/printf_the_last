/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 20:47:17 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 22:01:38 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int		ft_count_len(long long n, int i)
{
	while (n > 9)
	{
		n /= 10;
		i++;
	}
	return (i);
}

char			*ft_itoa(int n)
{
	long long	sign;
	int			i;
	char		*new_arr;

	i = 1;
	if (n < 0)
	{
		i++;
		sign = (long long)n * -1;
	}
	else
		sign = (long long)n;
	i = ft_count_len(sign, i);
	if ((new_arr = (char*)malloc(sizeof(char) * (i + 1))))
	{
		new_arr[i] = '\0';
		new_arr[--i] = (sign % 10) + '0';
		i -= 1;
		while ((sign /= 10) > 0)
			new_arr[i--] = (sign % 10) + '0';
		if (n < 0)
			new_arr[i] = '-';
		return (new_arr);
	}
	return (NULL);
}
