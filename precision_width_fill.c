/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   precision_width_fill.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/16 18:25:40 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 21:35:30 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_defarg	*ft_fill_precision(t_defarg *des_arg)
{
	int		i;
	int		j;
	char	*utility_str;

	if (ft_strlen(des_arg->str) > 1 && ft_strrchr(des_arg->str, '.'))
	{
		i = (int)ft_strlen(des_arg->str) - 1;
		while (i >= 0)
		{
			if (des_arg->str[i - 1] == '.')
				break ;
			i--;
		}
		j = i;
		while (ft_isdigit(des_arg->str[j]))
			j++;
		utility_str = ft_strsub(des_arg->str, i, (size_t)(j - i));
		des_arg->prec = ft_atoi(utility_str);
	}
	return (des_arg);
}

t_defarg	*ft_fill_width(t_defarg *des_arg)
{
	int		i;
	int		j;

	i = (int)ft_strlen(des_arg->str) - 1;
	while (i >= 0)
	{
		if (ft_isdigit(des_arg->str[i]))
		{
			j = i + 1;
			while (ft_isdigit(des_arg->str[i]))
			{
				if ((!ft_isdigit(des_arg->str[i - 1])
							&& des_arg->str[i - 1] != '.') || i == 0)
				{
					des_arg->wid = ft_atoi(ft_strsub(
											des_arg->str, i, (size_t)(j - i)));
					if (des_arg->wid > 0 || des_arg->wid < -1)
						return (des_arg);
				}
				i--;
			}
		}
		i--;
	}
	return (des_arg);
}
