# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mvorona <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/06 21:20:24 by mvorona           #+#    #+#              #
#    Updated: 2017/03/06 22:15:14 by mvorona          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


NAME = libftprintf.a

SRC = conversion_actions.c\
	  conversion_head.c\
	  decemical_int_print.c\
	  extra_funcs.c\
	  fill_modificators.c\
	  flags_funcs.c\
	  ft_itoa_base.c\
	  ft_itoa_base_unsigned.c\
	  ft_printf.c\
	  ft_util_arr.c\
	  digital_print_actions.c\
	  pf_head.c\
	  precision_width_fill.c\
	  print_funcs.c\
	  write_digital_conv_in_struct_argum.c\
	  write_str_chr_argum_int_str.c\
	  ft_pos_decemic_plus_space_print.c\
	  ft_print_plus_space.c\
	  ft_pointer_adress_func.c\
	  ft_unbehaviour_print.c\
	  ft_undefined_behaviour.c\
	  ft_strdup_wint.c\
	  neg_decemic_print.c\
	  ft_atoi.c\
	  ft_isalpha.c\
	  ft_isdigit.c\
	  ft_itoa.c\
	  ft_memset.c\
	  ft_strchr.c\
	  ft_strrchr.c\
	  ft_strcmp.c\
	  ft_strdup.c\
	  ft_strjoin.c\
	  ft_strlen.c\
	  ft_strnew.c\
	  ft_strstr.c\
	  ft_strsub.c\
	  ft_tolower.c\
	  ft_putchar.c\
	  ft_strcpy.c\
	  ft_strdel.c

OBJ = $(SRC:.c=.o)
HEAD = ft_printf.h
FLAG = -c -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

%.o: %c $(HEAD)
	gcc -o $(NAME) $(FLAG) $@ $<

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all
