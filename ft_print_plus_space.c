/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_plus_space.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvorona <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/06 20:51:15 by mvorona           #+#    #+#             */
/*   Updated: 2017/03/06 20:51:17 by mvorona          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_print_plus_space(t_defarg *des_arg)
{
	if (des_arg->flag_plus == 1)
	{
		ft_putchar_g('+');
		des_arg->flag_plus = 0;
		des_arg->wid--;
	}
	else if (des_arg->flag_space == 1)
	{
		ft_putchar_g(' ');
		des_arg->flag_space = 0;
		des_arg->wid--;
	}
}

void	ft_print_zero_space(t_defarg *des_arg)
{
	if (des_arg->flag_zero == 1)
		ft_putchar_g('0');
	else
		ft_putchar_g(' ');
	des_arg->wid--;
}
